## Midterm #2 - Code

In this program you will implement a class to store a list of natural numbers.
Natural Numbers are positive integers, not including zero. The class will be
named `NaturalList`. This class will internally store the list as a dynamic array.

It will give you the ability to:

* Add information to the list
* Get an element from the list
* Output the list as a comma separated list
* Get the size of the list
* Get the capacity of the list
* Get the sum of the values in the list
* Get the average of the values in the list
* Empty the list

It should have three member variables:

* One to hold the dynamic array (aka a pointer)
* One to hold the current size of the list
* One to hold the maximum capacity of the list.

### Member Functions

#### Constructor
----
Creates a dynamic array of unsigned integers of the given capacity. Initializes the
empty list and sets the maximum capacity member variable to the given capacity.

    @param unsigned int - The maximum capacity of the list.

#### Destructor
----
Frees any dynamic memory created in your class.

#### Add
----
Adds an item to the first open spot in the array (the end of the "list"). If the
list is full or if the value is not a natural number do not add and return
false.

    @param unsigned int - The value to add.
    @return bool - True if there was room to add, False if the list is full or if
                   the number is not a natural number.`

#### GetNumber
----
Returns the natural number at the given index.
If the index is not valid return 0.

    @param unsigned int - The index of the number to get
    @return unsigned int - The natural number at the given index

#### ToString
----
Creates a comma separated string of all of the values in the list. i.e.
"1, 5, 9, 3, 2". If the list is empty return "List Empty".

    @return string - A comma separated string or "List Empty".

#### GetSize
----
Returns the current size of the list.

    @return unsigned int - The size of the list.

#### GetCapacity
----
Returns the current capacity of the list.

    @return unsigned int - The capacity of the list.

#### Sum
----
Returns the sum of the items in the list. If the list is empty return 0.

    @return unsigned int - The sum, or 0 on empty.

#### Average
----
Returns the mean average of the items in the list.
If the list is empty return 0.

    @return double - The mean average as a floating-point.

#### Clear
----
Empties the list and recreates it at the indicated capacity. Capacity must be at least 1.
If capacity is less than 1 recreate it with a capacity of 10.

    @param unsigned int - The new capacity of the list.


### Notes

Your files should be named `natural_list.h` and `natural_list.cpp`

A Makefile has been included for this Midterm. Your options are:

    $ make (Tries to compile your exercise)
    $ make style (Checks for common styling mistakes)
    $ make clean (Removes the executable)

A Program that DOES NOT COMPILE will still be graded but will lose points.

NOTE: Warnings will also lose you points.

Good Luck!